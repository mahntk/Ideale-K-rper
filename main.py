import tkinter as tk
import csv
from PIL import Image, ImageTk
from tkinter import Label, Entry, Button, messagebox, ttk
import random
import threading

global Gewicht
global Alter
global Körpergröße
global Geschlecht
global Wunschgewicht_variable_abnehmen
global Wunschgewicht_variable_zunehmen
global Kalorien
#global bmr_zunehmen_int
global bmr_abnehmen_int
global mahlzeiten_db

class Applikation(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Ideale Körper")
        self.geometry("1300x700")
        self.resizable(True, True)
        #2
        self.begrüßungsfenster = BegrüßungsFenster(self)
        self.startScreen = StartScreen(self)
        self.zielAuswahlScreen = ZielAuswahlScreen(self)
        self.infoFenster = InfoFenster(self)

        self.fit = Fit(self)
        self.abnehmen = Abnehmen(self)
        self.zunehmen = Zunehmen(self)

        self.diätplan = Beispiel_Diätpalan(self)
        self.gericht = Beispiel_Gericht(self)

        self.diätplan_1 = Diätplan(self)
        self.sportplan = Sportplan(self)
        self.diätplan_sportplan = Diätplan_Sportplan(self)

        self.diätplan_ab = Diätplan_ab(self)
        self.sportplan_ab = Sportplan_ab(self)
        self.diätplan_sportplan_ab = Diätplan_Sportplan_ab(self)

        self.withdraw()
        self.benutzer_data = {}
        # 1
        self.begrüßungsfenster.deiconify()
class BegrüßungsFenster(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)

        self.title("Ideale Körper")
        self.geometry("1300x700")
        self.resizable(True, True)
        # Logo hinzufügen
        logo_path = r"Logo_für_erste_Seite.jpg"
        original_image = Image.open(logo_path)
        resized_image = original_image.resize((1300, 700))
        tk_image = ImageTk.PhotoImage(resized_image)

        logo_label = Label(self, image=tk_image)
        logo_label.image = tk_image
        logo_label.grid(row=0, column=0, pady=20)

        self.after(2500, self.wechsel_zu_StartScreen)

    def wechsel_zu_StartScreen(self):
        self.withdraw()  # Dieses Fenster ausblenden
        self.master.startScreen.deiconify()  # zeig Bildschirm



class StartScreen(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self,master )
        self.withdraw()
        self.withdraw()
        self.title("Ideale Körper")
        self.geometry("1300x700")
        self.resizable(True, True)

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)

        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        Wilkommen = tk.Label(self, text="Willkommen", font=("Cooper Black", 17, "italic bold"), bg="#383838", fg="#F5F5F5")
        Wilkommen.place(x=570, y=200)

        Zentral_rahmen = tk.Frame(self, bg="#383838")
        Zentral_rahmen.place(relx=0.5, rely=0.6,height=300,width=350, anchor='center')


        tk.Label(Zentral_rahmen, text="Name", font=("Cooper Black", 13, "italic bold"),bg='#7CCD7C', fg='#333333').grid(row=1, column=0, padx=7, pady=7)
        self.name_entry = tk.Entry(Zentral_rahmen, font=('Helvetica', 12))
        self.name_entry.grid(row=1, column=1, padx=2, pady=2)


        tk.Label(Zentral_rahmen, text="Alter", font=("Cooper Black", 13, "italic bold"),bg='#7CCD7C', fg='#333333').grid(row=2, column=0, padx=7, pady=7)
        self.alter_entry = tk.Entry(Zentral_rahmen, font=('Helvetica', 12))
        self.alter_entry.grid(row=2, column=1, padx=2, pady=2)


        tk.Label(Zentral_rahmen, text="Körpergröße", font=("Cooper Black", 13, "italic bold"),bg='#7CCD7C', fg='#333333').grid(row=3, column=0, padx=7, pady=7)
        self.Körpergröße_entry = tk.Entry(Zentral_rahmen, font=('Helvetica', 12))
        self.Körpergröße_entry.grid(row=3, column=1, padx=2, pady=2)

        tk.Label(Zentral_rahmen, text="Gewicht", font=("Cooper Black", 13, "italic bold"),bg='#7CCD7C', fg='#333333').grid(row=4, column=0, padx=7, pady=7)

        self.Gewicht_entry = tk.Entry(Zentral_rahmen, font=('Helvetica', 12))
        self.Gewicht_entry.grid(row=4, column=1, padx=2, pady=2)

        self.Geschlecht = tk.StringVar()
        tk.Label(Zentral_rahmen, text="Geschlecht", font=("Cooper Black", 13, "italic bold"),bg='#7CCD7C', fg='#333333').grid(row=5, column=0, padx=7,pady=7)
        self.geschlecht_entry = ttk.Combobox(Zentral_rahmen, textvariable= self.Geschlecht,
                                                 state='readonly')
        self.geschlecht_entry['values'] = ("Männlich", "Weiblich","Divers")
        self.geschlecht_entry.grid(row=5, column=1, columnspan=2, padx=2, pady=2)



        bestätigungs_button = tk.Button(Zentral_rahmen, text="Bestätigen", command=self.prüfe_eingabe, font=('Helvetica', 12),
                                         bg='#4CAF50', fg='#F5F5F5')
        bestätigungs_button.grid(row=7, column=1, pady=10)


        Info_button = tk.Button(Zentral_rahmen, text="Info", command= self.Info_zur_App, font=('Helvetica', 12),
                                        bg='#4CAF50', fg='#F5F5F5')
        Info_button.grid(row=8, column=0)

    def prüfe_eingabe(self):
        if not self.alle_felder_ausgefüllt():
            messagebox.showerror("Fehler", "Bitte füllen Sie alle Felder aus.")
            return

        if not self.ist_gültiges_name():
           messagebox.showerror("Fehler", "Ungültiges Name. Bitte geben Sie nür Buchstaben, max:50")
           return

        if not self.ist_gültiges_alter():
            messagebox.showerror("Fehler", "Ungültiges Alter. Bitte geben Sie eine Zahl zwischen 1 und 100 ein.")
            return

        if not self.ist_gültiges_Körpergröße():
            messagebox.showerror("Fehler", "Ungültiges Körpergröße. Bitte geben Sie eine Zahl zwischen 0 und 250 ein.")
            return

        if not self.ist_gültiges_Gewicht():
           messagebox.showerror("Fehler", "Ungültiges Gewicht. Bitte geben Sie eine Zahl zwischen 0 und 300 ein. und mit '.' statt ',' für dezimale Zahlen")
           return

        # if not geschlecht_entry:
        #    messagebox.showerror("Fehler", "Wählen sie bitte Ihr Gschlecht aus.")
        #    return

        if self.alle_felder_ausgefüllt():
            # Speichern wir Daten im Wörterbuch in der Hauptanwendungsklasse
            daten = {
                "Name": self.name_entry.get(),
                "Alter": self.alter_entry.get(),
                "Geschlecht": self.geschlecht_entry.get(),
                "Körpergröße": self.Körpergröße_entry.get(),
                "Gewicht": self.Gewicht_entry.get()
            }
            global Alter
            Alter = daten.get('Alter')
            global Gewicht
            Gewicht = daten.get('Gewicht')
            global Körpergröße
            Körpergröße = daten.get('Körpergröße')
            global Geschlecht
            Geschlecht = daten.get('Geschlecht')

            self.master.benutzer_daten = daten  # Zuweisung zum user_data-Wörterbuch in der Hauptanwendung

            messagebox.showinfo("Erfolg", "Vielen Dank für die Eingabe!")
            self.zu_ziel_auswahl()

    def zu_ziel_auswahl(self):
        self.withdraw()
        self.master.zielAuswahlScreen.deiconify()
    def alle_felder_ausgefüllt(self):
        return(
            self.name_entry.get() and self.alter_entry.get() and self.geschlecht_entry.get() and self.Körpergröße_entry.get() and self.Gewicht_entry.get())

    def ist_gültiges_name(self):
        try:
            name = str(self.name_entry.get())
            return 0 < len(name) <= 50 and name.isalpha()
        except ValueError:
            return False


    def ist_gültiges_alter(self):
        try:
            alter = int(self.alter_entry.get())
            return 0 < alter <= 100
        except ValueError:
            return False

    def ist_gültiges_Körpergröße(self):
        try:
            körpergröße = int(self.Körpergröße_entry.get())
            return 0 < körpergröße <= 250
        except ValueError:
            return False

    def ist_gültiges_Gewicht(self):
         try:
             Gewicht = float(self.Gewicht_entry.get())
             return 0 < Gewicht <= 300
         except ValueError:
             return False


    def Info_zur_App(self):
        self.withdraw()
        self.master.infoFenster.deiconify()



class ZielAuswahlScreen(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()
        self.title("Ideale Körper")

        self.geometry("1300x700")
        self.resizable(True, True)



        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        Zentral_rahmen1 = tk.Frame(self, bg="#383838")
        Zentral_rahmen1.place(relx=0.5, rely=0.6, anchor='center')

        tk.Label(Zentral_rahmen1, text="Wählen Sie Ihr Ziel aus", font=("Cooper Black", 17, "italic bold"),bg="#383838", fg="#F5F5F5").pack(pady=20)

        # Dropdown-Menü für die Auswahl
        self.zielvariabeln = tk.StringVar()
        ziel_Auswahl = ttk.Combobox(Zentral_rahmen1, textvariable=self.zielvariabeln, state='readonly')
        ziel_Auswahl['values'] = ('Fit bleiben', 'Zunehmen', 'Abnehmen')
        ziel_Auswahl.pack(fill='x', padx=50, pady=10)

        # Bestätigungsbutton
        bestätigen_knopf = tk.Button(Zentral_rahmen1, text="Bestätigen", command=self.bereite_vor, bg='#4CAF50',
                                   fg='#F5F5F5')
        bestätigen_knopf.pack(fill='x', padx=50, pady=10)

        # Zurück-Button
        zurück = tk.Button(Zentral_rahmen1, text="Zurück zur Eingabe", command=self.zurück_zum_StartScreen,
                           bg='#4CAF50', fg='#F5F5F5', font=('Helvetica', 12))
        zurück.pack(fill='x', padx=50, pady=70)

    def bereite_vor(self):
        ziel = self.zielvariabeln.get()
        if ziel == 'Fit bleiben':
            self.withdraw()
            self.master.fit.deiconify()
        elif ziel == 'Zunehmen':
            self.withdraw()
            self.master.zunehmen.deiconify()
        elif ziel == 'Abnehmen':
            self.withdraw()
            self.master.abnehmen.deiconify()
        else:
            messagebox.showerror("Fehler","Bitte ein Wahl treffen")

    def zurück_zum_StartScreen(self):
        self.withdraw()
        self.master.startScreen.deiconify()


class InfoFenster(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title("Ideale Körper")
        self.geometry('1300x700')
        self.resizable(True, True)

        hintergrund_link = r"InfoF.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)

        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        label_bild = Label(self, image=self.hintergrund_bild)
        label_bild.place(x=0, y=0, relwidth=1, relheight=1)

        Textfeld = Label(self, text="Wie sieht Dein Traumkörper aus? Willst Du ein paar Kilos zunehmen,"
                                    " oder doch ein paar Kilos weniger haben?\n Einen muskulösen, "
                                    "oder doch lieber athletischen Körper haben?\n "
                                    "Vielleicht denkst Du dir jetzt aber auch, "
                                    "das Du deinen Traum Body doch schon längst hast. \n \n"
                                    "Aber egal Wir haben für jeden von Euch etwas dabei! \n"
                                    "Wir erstellen Dir, einen auf Dich angepassten Diätplan,\n "
                                    "mit dem Du deinen Idealen Körper schon bald im Spiegel betrachten "
                                    "kannst.\n Und für die Sprotler unter Euch, bieten Wir zudem noch Individuelle "
                                    "Fitnesspläne,\n die ganz Euren Diatplan unterstützen\n "
                                    "Falls Du diesen schon längst "
                                    "hast, aber nicht die Zeit um dir "
                                    "jeden Tag Gesunde Gerichte einfallen zu lassen,\n bieten Wir Dir hier "
                                    "bei 'Ideale Körper' ein abwechslungsreiches, "
                                    "gesundes  und ideal auf Dich abgewogenes Menü.\n Klicke nur auf 'Zurück "
                                    "zur Einagbe' und bestätige Deine Daten.\n Und schon kann die Journey beginnen",
                          font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333', justify="center")
        Textfeld.pack(padx=80, pady=200)

        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=600, y=600)

        zurück_button = Button(frame, text='Zurück zur Eingabe', bg='#4CAF50', fg='#F5F5F5',
                                command=self.zurück_zu_StartScreen, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True)

    def zurück_zu_StartScreen(self):
        self.withdraw()
        self.master.startScreen.deiconify()

class Fit(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title("Fit bleiben")
        self.geometry('1300x700')

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        Textfeld = Label(self, text="Wir helfen Ihnen Fit zu bleiben", font=("Cooper Black", 13, "italic bold"),
                         bg='#7CCD7C', fg='#333333', justify="center")
        Textfeld.pack(padx=80, pady=200)

        Zentral_rahmen1 = tk.Frame(self, bg="#383838")
        Zentral_rahmen1.place(relx=0.5, rely=0.6, anchor='center')

        tk.Label(Zentral_rahmen1, text="Fit bleiben mit:", font=("Cooper Black", 17, "italic bold"),bg="#383838", fg="#F5F5F5").pack(pady=20)

        self.auswahlvariable = tk.StringVar()
        self.ziel_Auswahl = ttk.Combobox(Zentral_rahmen1, textvariable=self.auswahlvariable,  state='readonly') # +command=self.combobox_grün
        self.ziel_Auswahl['values'] = ('Diätplan', 'Einzelne Gerichte')
        self.ziel_Auswahl.pack(fill='x', padx=50, pady=10)

        tk.Label(Zentral_rahmen1, text="Lebensmittel die Sie nicht vertragen:", font=("Cooper Black", 10, "italic bold"),bg="#383838", fg="#F5F5F5").pack(pady=20)
        Unverträgliche_lebensmittel = tk.Entry(Zentral_rahmen1, font=('Helvetica', 9))
        Unverträgliche_lebensmittel.pack(fill='x', padx=50, pady=10)

        bestätigen_knopf = tk.Button(Zentral_rahmen1, text="Bestätigen", command=self.diätplan_or_gericht, bg='#4CAF50',
                                     fg='#F5F5F5', font=('Helvetica', 12))
        bestätigen_knopf.pack(fill='x', padx=50, pady=10)

        zurück_button = Button(Zentral_rahmen1, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                               command=self.zu_ziel_auswahl, font=('Helvetica', 12))
        zurück_button.pack(fill='x', padx=50, pady=10)

    def diätplan_or_gericht(self):
        Auswahl = self.auswahlvariable.get()
        if Auswahl == 'Diätplan':
            self.withdraw()
            self.master.diätplan.deiconify()
        else:
            self.withdraw()
            self.master.gericht.deiconify()

    def zu_ziel_auswahl(self):
        self.withdraw()
        self.master.zielAuswahlScreen.deiconify()

class Beispiel_Diätpalan(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title('Diätplan')
        self.geometry('1300x700')

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        self.mahlzeiten_db = self.lese_mahlzeiten_csv()

        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=350, y=500)

        berechnen_button = Button(frame, text='Kalorienbedarf berechnen', bg='#4CAF50', fg='#F5F5F5',
                                  command=self.berechne_kalorienbedarf, font=('Helvetica', 12))
        berechnen_button.pack(fill=tk.BOTH, expand=True, pady=(20, 10))  # Abstand oben 20, unten 10

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                               command=self.zurück_zu_Fit, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True, pady=(10, 20))  # Abstand oben 10, unten 20

        label_text = "Geben Sie Unverträglichkeiten ein (durch Komma getrennt):"
        Label(frame, text=label_text, font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333').pack()

        self.unverträglichkeiten_entry = Entry(frame, font=('Helvetica', 12), width=30)
        self.unverträglichkeiten_entry.pack(pady=(10, 10))

    def lese_mahlzeiten_csv(self):
        dateipfad = r'zutaten.csv2'
        mahlzeiten_db = {}
        with open(dateipfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                typ = zeile['Typ']
                mahlzeit = zeile['Mahlzeit']
                self.kalorien = int(zeile['Kalorien'])
                zutaten = zeile['Zutaten'].split('|')
                if typ not in mahlzeiten_db:
                    mahlzeiten_db[typ] = []
                mahlzeiten_db[typ].append((mahlzeit, self.kalorien, zutaten))
        return mahlzeiten_db


        global Kalorien
        Kalorien = self.kalorien.get()



    def berechne_kalorienbedarf(self):
        # Zugriff auf Benutzerdaten aus dem Hauptfenster
        geschlecht = self.master.benutzer_daten['Geschlecht']
        große = float(self.master.benutzer_daten['Körpergröße'])
        self.gewicht = float(self.master.benutzer_daten['Gewicht'])
        alter = int(self.master.benutzer_daten['Alter'])
        global bmr_int

        if geschlecht == "Männlich":
            bmr = (88.362 + (13.397 * self.gewicht) + (4.799 * große) - (5.677 * alter))
        else:
            bmr = (447.593 + (9.247 * self.gewicht) + (3.098 * große) - (4.330 * alter))

        unverträglichkeiten_text = self.unverträglichkeiten_entry.get()
        unverträglichkeiten_liste = [item.strip() for item in unverträglichkeiten_text.split(',') if item]

        self.bmr_int = int(bmr)

        # Update der Benutzerdaten
        self.master.benutzer_daten['Unverträglichkeiten'] = unverträglichkeiten_liste
        diätplan = self.erstelle_diätplan( self.bmr_int,self.master.benutzer_daten)
        self.zeige_plan( diätplan)

    def wähle_mahlzeit(self, mahlzeiten, zielkalorien, unverträglichkeiten):
        passende_mahlzeiten = [m for m in mahlzeiten if
                               m[1] <= zielkalorien and not any(allergen in m[2] for allergen in unverträglichkeiten)]
        return random.choice(passende_mahlzeiten) if passende_mahlzeiten else ("Nicht verfügbar", 0)

    def erstelle_diätplan(self, kalorienbedarf, benutzerdaten):
        unverträglichkeiten = benutzerdaten.get('Unverträglichkeiten', [])
        self.diätplan = {"Frühstück": None, "Mittagessen": None, "Abendessen": None,
                    "Desserts": None, "Zwischenmahlzeiten": None}

        for mahlzeit_typ in self.diätplan.keys():
            try:
                verfügbare_mahlzeiten = self.mahlzeiten_db[mahlzeit_typ]
                mahlzeit = self.wähle_mahlzeit(verfügbare_mahlzeiten, kalorienbedarf / 3, unverträglichkeiten)
                self.diätplan[mahlzeit_typ] = mahlzeit
            except KeyError:
                print(f"Warnung: Keine Daten für {mahlzeit_typ} gefunden")

        return self.diätplan

    def zeige_plan(self , diätplan):
        plan_window = tk.Toplevel(self)
        plan_window.title("Ihr Plan")
        plan_window.geometry('1300x700')
        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        plan_window.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(plan_window, image=plan_window.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(relx=0.5, rely=0.5, anchor='center')
        #
        # zurück_button = Button(frame2, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
        #                        command=self.zurück2, font=('Helvetica', 12))
        # zurück_button.pack(fill=tk.BOTH, expand=True, pady=(10, 20))



        diätplan_text = (
            f"Ihr täglicher Kalorienbedarf, um {self.gewicht} Kg zu behalten, beträgt: {self.bmr_int:.2f} kcal\n")

        MAX_ITERATIONS = 100    # Maximale Anzahl von Interatinen zur Generierung eines neuen Plan
        iteration_count = 0      #Interationszähler

        while True:
            self.gesamtkalorien_mahlzeiten = sum(
                mahlzeit_info[1] for mahlzeit_info in self.diätplan.values() if mahlzeit_info and mahlzeit_info[0]
            )

            if (self.bmr_int - 200) <= self.gesamtkalorien_mahlzeiten <= (self.bmr_int + 200):
                for mahlzeit_typ, mahlzeit_info in self.diätplan.items():
                    if mahlzeit_info is None or mahlzeit_info[0] is None:
                        diätplan_text += f"{mahlzeit_typ}: Nicht verfügbar\n"
                    else:
                        diätplan_text += f"{mahlzeit_typ}: {mahlzeit_info[0]} ({mahlzeit_info[1]} kcal)\n"

                Textfeld = Label(plan_window, text=diätplan_text, font=("Cooper Black", 13, "italic bold"),
                                 bg='#7CCD7C',
                                 fg='#333333', justify="center")
                Textfeld.pack(padx=80, pady=200)

                break

            else:
         # ein neuer Versuch einer Erzeugung eines Diätplan
                diätplan = self.erstelle_diätplan( self.bmr_int,self.master.benutzer_daten)
                iteration_count += 1

            if iteration_count >= MAX_ITERATIONS:
                 print("Maximale Anzahl von Iterationen erreicht. Es konnte kein Plan gefunden werden.")
                 break


    def zurück_zu_Fit(self):
        self.withdraw()
        self.master.fit.deiconify()



class Beispiel_Gericht(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title('Gericht')
        self.geometry('1300x700')

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        self.Zentral_rahmen = tk.Frame(self, bg="#383838")#
        self.Zentral_rahmen.place(relx=0.5, rely=0.5, anchor='center')

        self.frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        self.frame.place(x=600, y=600)

        zurück_button = Button(self.frame, text='Zurück zur Eingabe', bg='#4CAF50', fg='#F5F5F5',
                               command=self.zurück_zu_Fit, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True)

        self.anzeigen_mahlzeiten()

    def anzeigen_mahlzeiten(self):
        mahlzeiten_db = self.lese_mahlzeiten_csv()

        diätplan_text = "\n\n".join([f"{typ}:\n{', '.join([f'{mahlzeit[0]} ({mahlzeit[1]} kcal) {mahlzeit[2]} \n' for mahlzeit in mahlzeiten])}" for typ, mahlzeiten in mahlzeiten_db.items()])
        Textfeld = Label(self.Zentral_rahmen, text=diätplan_text, font=("Cooper Black", 13, "italic bold"),
                     bg='#7CCD7C', fg='#333333', justify="center")
        Textfeld.pack(padx=80, pady=200)

    def lese_mahlzeiten_csv(self):
        dateipfad = r'zutaten.csv2'
        mahlzeiten_db = {}
        with open(dateipfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                typ = zeile['Typ']
                mahlzeit = zeile['Mahlzeit']
                kalorien = int(zeile['Kalorien'])
                zutaten = zeile['Zutaten'].split('|')
                if typ not in mahlzeiten_db:
                    mahlzeiten_db[typ] = []
                mahlzeiten_db[typ].append((mahlzeit, kalorien, zutaten))
        return mahlzeiten_db

    def zurück_zu_Fit(self):
            self.withdraw()
            self.master.fit.deiconify()

class Abnehmen(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)

        self.withdraw()

        self.title("Abnehmen")
        self.geometry('1300x700')

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        Textfeld = Label(self,text="Wir helfen Ihnen Abzunehmen" ,font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333', justify="center")
        Textfeld.pack(padx=80, pady=200)

        self.Zentral_rahmen = tk.Frame(self, bg="#383838")
        self.Zentral_rahmen.place(relx=0.5, rely=0.6, anchor='center')
        # wunschgewicht label und Entry
        self.Wunschgewicht_variable = tk.StringVar()
        tk.Label(self.Zentral_rahmen, text="*Wunschgewicht:",
                 font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
                 fg='#333333').grid(row=1, column=0, padx=7, pady=7)

        self.Wunschgewicht_variable = tk.StringVar()
        self.wunschgewicht_entry = tk.Entry(self.Zentral_rahmen, textvariable=self.Wunschgewicht_variable, font=('Helvetica', 12))
        self.wunschgewicht_entry.grid(row=1, column=1, padx=2, pady=2)

        # Erste Combobox ob mit Diätplan Zunehmen ider ohne
        tk.Label(self.Zentral_rahmen, text="Mit Diätplan?", font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
                 fg='#333333').grid(row=3, column=0, padx=7, pady=7)
        self.Diätplan_auswahlvariable = tk.StringVar()
        self.Diätplan_auswhal = ttk.Combobox(self.Zentral_rahmen, textvariable=self.Diätplan_auswahlvariable,
                                             state='readonly')
        self.Diätplan_auswhal['values'] = ('Ja', 'Nein')
        self.Diätplan_auswhal.grid(row=3, column=1, padx=2, pady=2)
        # Zweite Combobox ob mit Sportplan zunehmen oder ohne
        tk.Label(self.Zentral_rahmen, text="Mit Sportplan?", font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
                 fg='#333333').grid(row=4, column=0, padx=7, pady=7)
        self.Sportplan_auswahlvariable = tk.StringVar()
        self.Sportplan_auswahl = ttk.Combobox(self.Zentral_rahmen, textvariable=self.Sportplan_auswahlvariable,
                                              state='readonly')
        self.Sportplan_auswahl['values'] = ('Ja', 'Nein')
        self.Sportplan_auswahl.grid(row=4, column=1, padx=2, pady=2)


        bestätigen_knopf = tk.Button(self.Zentral_rahmen, text="Bestätigen", command=self.select, bg='#4CAF50',
                                     fg='#F5F5F5', font=('Helvetica', 12))
        bestätigen_knopf.grid(row=5, column=0, columnspan=2, padx=2, pady=2)

        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=600, y=600)

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                               command=self.zu_ziel_auswahl, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True)


    def select(self):
        wunschgewicht_value = self.Wunschgewicht_variable.get()

        if not wunschgewicht_value:
            messagebox.showerror("Fehler", "Bitte füllen Sie das Wunschgewicht aus.")
            return  # Beenden der Methode, wenn kein Wunschgewicht angegeben wurde

        try:
            wunschgewicht = float(wunschgewicht_value)
            aktuelles_gewicht = float(self.master.benutzer_daten['Gewicht'])

            if wunschgewicht >= aktuelles_gewicht:
                messagebox.showerror("Fehler", "Wunschgewicht muss kleiner als aktuelles Gewicht sein.")
                return  # Beenden der Methode, wenn das Wunschgewicht ungültig ist

            # Wenn das Wunschgewicht gültig ist, führen Sie weitere Aktionen durch.
            messagebox.showinfo("Erfolg", "bist du bereit?")
            # Fortsetzung des Codes zur Verarbeitung des Wunschgewichts

        except ValueError:
            messagebox.showerror("Fehler", "Bitte geben Sie ein gültiges Gewicht ein.")
            return

        daten = {
            "Wunschgewicht_ab": self.wunschgewicht_entry.get(),

        }

        global Wunschgewicht_variable_abnehmen
        Wunschgewicht_variable_abnehmen = daten.get('Wunschgewicht_ab')

        selected_value_Diätplan = self.Diätplan_auswahlvariable.get()
        selected_value_Sportplan = self.Sportplan_auswahlvariable.get()

        self.Einschränkungen_auswahl = ttk.Combobox(self.Zentral_rahmen,
                                                    state='readonly')
        self.Einschränkungen_auswahl['values'] = ('Knieprobleme', 'Wirbelsäule', 'Asma')
        self.Einschränkungen_auswahl.grid(row=6, column=0, columnspan=2, padx=2, pady=2)

        self.Lebensmittel_auswahl = ttk.Combobox(self.Zentral_rahmen,
                                                 state='readonly')
        self.Lebensmittel_auswahl['values'] = ('Zwiebeln', 'Lactose')
        self.Lebensmittel_auswahl.grid(row=7, column=0, columnspan=2, padx=2, pady=2)

        # Überprüfe, ob die ausgewählte Option diejenige ist, bei der die Entry-Box erscheinen soll
        if selected_value_Diätplan == 'Ja' and selected_value_Sportplan == 'Nein':
            self.Einschränkungen_auswahl.destroy()

            self.leeres_label_1 = tk.Label(self.Zentral_rahmen, text="", bg="#383838")
            self.leeres_label_1.grid(row=6, column=0)

            self.Lebensmittel_auswahl.grid(row=7, column=0, columnspan=2, padx=2, pady=2)

            self.withdraw()
            self.master.diätplan_ab.deiconify()




        elif selected_value_Diätplan == 'Ja' and selected_value_Sportplan == 'Ja':

            self.Einschränkungen_auswahl.grid(row=6, column=0, columnspan=2, padx=2, pady=2)
            self.Lebensmittel_auswahl.grid(row=7, column=0, columnspan=2, padx=2, pady=2)

            self.withdraw()
            self.master.diätplan_sportplan_ab.deiconify()


        elif selected_value_Diätplan == 'Nein' and selected_value_Sportplan == 'Ja':
            self.Lebensmittel_auswahl.destroy()

            self.Einschränkungen_auswahl.grid(row=6, column=0, columnspan=2, padx=2, pady=2)
            self.leeres_label_2 = tk.Label(self.Zentral_rahmen, text="", bg="#383838")

            self.leeres_label_2.grid(row=7, column=0)

            self.withdraw()
            self.master.sportplan_ab.deiconify()



        else:
            self.Lebensmittel_entry.destroy()
            self.einschränkungen_entry.destroy()
            messagebox.showerror("Fehler",
                                 "Bitte geben Sie an, ob Sie mit Diätplan, Sportplan oder Beiden in Kombination "
                                 "zunehmen wollen!")
            return



    def zu_ziel_auswahl(self):
        self.withdraw()
        self.master.zielAuswahlScreen.deiconify()




class Zunehmen(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title("Zunehmen")
        self.geometry('1300x700')

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        Textfeld = Label(self,text="Wir helfen Ihnen Zuzunehmen", font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333', justify="center")
        Textfeld.pack(padx=80, pady=200)

        self.Zentral_rahmen = tk.Frame(self, bg="#383838")
        self.Zentral_rahmen.place(relx=0.5, rely=0.6, anchor='center')
        # wunschgewicht label und Entry
        self.Wunschgewicht_variable = tk.StringVar()
        tk.Label(self.Zentral_rahmen, text="*Wunschgewicht:",
                 font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
                 fg='#333333').grid(row=1, column=0, padx=7, pady=7)
        self.wunschgewicht_entry = tk.Entry(self.Zentral_rahmen, textvariable=self.Wunschgewicht_variable, font=('Helvetica', 12))
        self.wunschgewicht_entry.grid(row=1, column=1, padx=2, pady=2)

        # Erste Combobox ob mit Diätplan Zunehmen ider ohne
        tk.Label(self.Zentral_rahmen, text="Mit Diätplan?", font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
                 fg='#333333').grid(row=3, column=0, padx=7, pady=7)
        self.Diätplan_auswahlvariable = tk.StringVar()
        self.Diätplan_auswhal = ttk.Combobox(self.Zentral_rahmen, textvariable=self.Diätplan_auswahlvariable,
                                         state='readonly')
        self.Diätplan_auswhal['values'] = ('Ja', 'Nein')
        self.Diätplan_auswhal.grid(row=3, column=1, padx=2, pady=2)
        # Zweite Combobox ob mit Sportplan zunehmen oder ohne
        tk.Label(self.Zentral_rahmen, text="Mit Sportplan?", font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
                 fg='#333333').grid(row=4, column=0, padx=7, pady=7)
        self.Sportplan_auswahlvariable = tk.StringVar()
        self.Sportplan_auswahl = ttk.Combobox(self.Zentral_rahmen, textvariable=self.Sportplan_auswahlvariable,
                                         state='readonly')
        self.Sportplan_auswahl['values'] = ('Ja', 'Nein')
        self.Sportplan_auswahl.grid(row=4, column=1, padx=2, pady=2)

        bestätigen_knopf = tk.Button(self.Zentral_rahmen, text="Bestätigen", command=self.select, bg='#4CAF50',
                                     fg='#F5F5F5', font=('Helvetica', 12))
        bestätigen_knopf.grid(row=5, column=0, columnspan=2, padx=2, pady=2)



        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=600, y=600)

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                               command=self.zu_ziel_auswahl, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True)

    def select(self):
        wunschgewicht_value = self.Wunschgewicht_variable.get()

        if not wunschgewicht_value:
            messagebox.showerror("Fehler", "Bitte füllen Sie das Wunschgewicht aus.")
            return  # Beenden der Methode, wenn kein Wunschgewicht angegeben wurde

        try:
            wunschgewicht = float(wunschgewicht_value)
            aktuelles_gewicht = float(self.master.benutzer_daten['Gewicht'])

            if wunschgewicht <= aktuelles_gewicht:
                messagebox.showerror("Fehler", "Wunschgewicht muss großer als aktuelles Gewicht sein.")
                return  # Beenden der Methode, wenn das Wunschgewicht ungültig ist

            # Wenn das Wunschgewicht gültig ist, führen Sie weitere Aktionen durch.
            messagebox.showinfo("Erfolg", "bist du Bereit?")
            # Fortsetzung des Codes zur Verarbeitung des Wunschgewichts

        except ValueError:
            messagebox.showerror("Fehler", "Bitte geben Sie ein gültiges Gewicht ein.")
            return

        daten = {
            "Wunschgewicht_zu": self.wunschgewicht_entry.get(),

        }

        global Wunschgewicht_variable_zunehmen
        Wunschgewicht_variable_zunehmen = daten.get('Wunschgewicht_zu')

        selected_value_Diätplan = self.Diätplan_auswahlvariable.get()
        selected_value_Sportplan = self.Sportplan_auswahlvariable.get()

        self.Einschränkungen_auswahl = ttk.Combobox(self.Zentral_rahmen,
                                                    state='readonly')
        self.Einschränkungen_auswahl['values'] = ('Knieprobleme', 'Wirbelsäule', 'Asma')
        self.Einschränkungen_auswahl.grid(row=6, column=0, columnspan=2, padx=2, pady=2)

        self.Lebensmittel_auswahl = ttk.Combobox(self.Zentral_rahmen,
                                                 state='readonly')
        self.Lebensmittel_auswahl['values'] = ('Zwiebeln', 'Lactose')
        self.Lebensmittel_auswahl.grid(row=7, column=0, columnspan=2, padx=2, pady=2)

        # Überprüfe, ob die ausgewählte Option diejenige ist, bei der die Entry-Box erscheinen soll
        if selected_value_Diätplan == 'Ja' and selected_value_Sportplan == 'Nein':
            self.Einschränkungen_auswahl.destroy()

            self.leeres_label_1 = tk.Label(self.Zentral_rahmen, text="", bg="#383838")
            self.leeres_label_1.grid(row=6, column=0)

            self.Lebensmittel_auswahl.grid(row=7, column=0, columnspan=2, padx=2, pady=2)

            self.withdraw()
            self.master.diätplan_1.deiconify()




        elif selected_value_Diätplan == 'Ja' and selected_value_Sportplan == 'Ja':

            self.Einschränkungen_auswahl.grid(row=6, column=0, columnspan=2, padx=2, pady=2)
            self.Lebensmittel_auswahl.grid(row=7, column=0, columnspan=2, padx=2, pady=2)

            self.withdraw()
            self.master.diätplan_sportplan.deiconify()


        elif selected_value_Diätplan == 'Nein' and selected_value_Sportplan == 'Ja':
            self.Lebensmittel_auswahl.destroy()

            self.Einschränkungen_auswahl.grid(row=6, column=0, columnspan=2, padx=2, pady=2)
            self.leeres_label_2 = tk.Label(self.Zentral_rahmen, text="", bg="#383838")

            self.leeres_label_2.grid(row=7, column=0)

            self.withdraw()
            self.master.sportplan.deiconify()



        else:
            self.Lebensmittel_entry.destroy()
            self.einschränkungen_entry.destroy()
            messagebox.showerror("Fehler",
                                 "Bitte geben Sie an, ob Sie mit Diätplan, Sportplan oder Beiden in Kombination "
                                 "zunehmen wollen!")
            return



    def zu_ziel_auswahl(self):
        self.withdraw()
        self.master.zielAuswahlScreen.deiconify()

class Diätplan(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title('Diätplan fürs Zunehmen')
        self.geometry('1300x700')

        self.mahlzeiten_db = self.lese_mahlzeiten_csv()
        #self.gesamtkalorien_mahlzeiten = int()

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=350, y=500)

        berechnen_button = Button(frame, text='Kalorienbedarf berechnen', bg='#4CAF50', fg='#F5F5F5',
                                  command=self.berechne_kalorienbedarf, font=('Helvetica', 12))
        berechnen_button.pack(fill=tk.BOTH, expand=True, pady=(20, 10))  # Abstand oben 20, unten 10

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                               command=self.zurück, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True, pady=(10, 20))  # Abstand oben 10, unten 20

        label_text = "Geben Sie Unverträglichkeiten ein (durch Komma getrennt):"
        Label(frame, text=label_text, font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333').pack()

        self.unverträglichkeiten_entry = Entry(frame, font=('Helvetica', 12), width=30)
        self.unverträglichkeiten_entry.pack(pady=(10, 10))

    def lese_mahlzeiten_csv(self):
        dateipfad = r'zutaten.csv2'
        mahlzeiten_db = {}
        with open(dateipfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                typ = zeile['Typ']
                mahlzeit = zeile['Mahlzeit']
                kalorien = int(zeile['Kalorien'])
                zutaten = zeile['Zutaten'].split('|')
                if typ not in mahlzeiten_db:
                    mahlzeiten_db[typ] = []
                mahlzeiten_db[typ].append((mahlzeit, kalorien, zutaten))
        return mahlzeiten_db


        # global Kalorien
        # Kalorien = self.kalorien.get()



    def berechne_kalorienbedarf(self):
        # Zugriff auf Benutzerdaten aus dem Hauptfenster
        geschlecht = self.master.benutzer_daten['Geschlecht']
        große = float(self.master.benutzer_daten['Körpergröße'])
        gewicht = float(self.master.benutzer_daten['Gewicht'])
        alter = int(self.master.benutzer_daten['Alter'])

        if geschlecht == "Männlich":
            bmr_zunehmen = (88.362 + (13.397 * gewicht) + (4.799 * große) - (5.677 * alter))+500
        else:
            bmr_zunehmen = (447.593 + (9.247 * gewicht) + (3.098 * große) - (4.330 * alter))+500

        unverträglichkeiten_text = self.unverträglichkeiten_entry.get()
        unverträglichkeiten_liste = [item.strip() for item in unverträglichkeiten_text.split(',') if item]

        self.bmr_zunehmen_int = int(bmr_zunehmen)

        # Update der Benutzerdaten
        self.master.benutzer_daten['Unverträglichkeiten'] = unverträglichkeiten_liste
        diätplan = self.erstelle_diätplan( self.bmr_zunehmen_int,self.master.benutzer_daten)
        self.zeige_plan( diätplan)

    def wähle_mahlzeit(self, mahlzeiten, zielkalorien, unverträglichkeiten):
        passende_mahlzeiten = [m for m in mahlzeiten if
                               m[1] <= zielkalorien and not any(allergen in m[2] for allergen in unverträglichkeiten)]
        return random.choice(passende_mahlzeiten) if passende_mahlzeiten else ("Nicht verfügbar", 0)

    def erstelle_diätplan(self, kalorienbedarf, benutzerdaten):
        unverträglichkeiten = benutzerdaten.get('Unverträglichkeiten', [])
        diätplan = {"Frühstück": None, "Mittagessen": None, "Abendessen": None,
                    "Desserts": None, "Zwischenmahlzeiten": None}

        for mahlzeit_typ in diätplan.keys():
            try:
                verfügbare_mahlzeiten = self.mahlzeiten_db[mahlzeit_typ]
                mahlzeit = self.wähle_mahlzeit(verfügbare_mahlzeiten, kalorienbedarf / 3, unverträglichkeiten)
                diätplan[mahlzeit_typ] = mahlzeit
            except KeyError:
                print(f"Warnung: Keine Daten für {mahlzeit_typ} gefunden")

        return diätplan

    def zeige_plan(self , diätplan):
        plan_window = tk.Toplevel(self)
        plan_window.title("Ihr Plan")
        plan_window.geometry('1300x700')
        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        plan_window.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(plan_window, image=plan_window.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        frame = tk.Frame(plan_window, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=350, y=500)

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                               command=self.zurück2, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True, pady=(10, 20))

        gewichtdifferenz =int(Wunschgewicht_variable_zunehmen) - int(Gewicht)
        Zeit_bis_Wunschgewicht = gewichtdifferenz / 0.5


        diätplan_text = (
            f"Ihr täglicher Kalorienbedarf, um {gewichtdifferenz} Kg zu erreichen, beträgt: {self.bmr_zunehmen_int:.2f} kcal\n"
            f"Sie erreichen Ihr Wunschgewicht in {Zeit_bis_Wunschgewicht} Wochen\n\nDiätplan:\n")

        MAX_ITERATIONS = 100    # Maximale Anzahl von Interatinen zur Generierung eines neuen Plan
        iteration_count = 0      #Interationszähler

        while True:
            self.gesamtkalorien_mahlzeiten = sum(
                mahlzeit_info[1] for mahlzeit_info in diätplan.values() if mahlzeit_info and mahlzeit_info[0]
            )
            if (self.bmr_zunehmen_int - 200) <= self.gesamtkalorien_mahlzeiten <= (self.bmr_zunehmen_int + 200):
                for mahlzeit_typ, mahlzeit_info in diätplan.items():
                    if mahlzeit_info is None or mahlzeit_info[0] is None:
                        diätplan_text += f"{mahlzeit_typ}: Nicht verfügbar\n"
                    else:
                        diätplan_text += f"{mahlzeit_typ}: {mahlzeit_info[0]} ({mahlzeit_info[1]} kcal)\n"

                Textfeld = Label(plan_window, text=diätplan_text, font=("Cooper Black", 13, "italic bold"),
                                 bg='#7CCD7C',
                                 fg='#333333', justify="center")
                Textfeld.pack(padx=80, pady=200)

                break

            else:
         # ein neuer Versuch einer Erzeugung eines Diätplan
                diätplan = self.erstelle_diätplan(self.bmr_zunehmen_int, self.master.benutzer_daten)
                iteration_count += 1

            if iteration_count >= MAX_ITERATIONS:
                print("Maximale Anzahl von Iterationen erreicht. Es konnte kein Plan gefunden werden.")
                break


    def zurück2(self):
         self.withdraw()
         self.master.diätplan_1.deiconify()


    def zurück(self):
        self.withdraw()
        self.master.zunehmen.deiconify()


class Sportplan(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title(' Sportplan fürs Zunehmne')
        self.geometry('1300x700')

        self.sport_csv_pfad = r'Wochenplan für Sport'

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)


        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=560, y=500)

        # Using a lambda function to pass an empty list for user restrictions
        sportplan_button = Button(frame, text='Sportplan generieren', bg='#4CAF50', fg='#F5F5F5',
                                  command=lambda: self.generiere_sportplan([]), font=('Helvetica', 12))
        sportplan_button.pack(fill=tk.BOTH, expand=True, pady=(20, 10))

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                               command=self.zurück, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True, pady=(10, 20))

    def lese_sport_csv(self):
        sport_db = {}
        with open(self.sport_csv_pfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                tag = zeile['Tag'].strip()
                if tag not in sport_db:
                    sport_db[tag] = []
                sport_db[tag].append(zeile)
        return sport_db

    def generiere_sportplan(self, benutzer_einschränkungen):
        sportaktivitäten = self.lese_sport_csv()
        sportplan = self.erstelle_sportplan(sportaktivitäten, benutzer_einschränkungen)
        self.zeige_sportplan(sportplan)

    def erstelle_sportplan(self, sportaktivitäten, benutzer_einschränkungen):
        sportplan = []
        for tag, aktivitäten in sportaktivitäten.items():
            geeignete_aktivitäten = [a for a in aktivitäten if not any(
                einschränkung in a['Einschränkungen'] for einschränkung in benutzer_einschränkungen)]
            if geeignete_aktivitäten:
                gewählte_aktivität = random.choice(geeignete_aktivitäten)
                sportplan.append(f"{tag}: {gewählte_aktivität['Aktivität']} ({gewählte_aktivität['Dauer']})")
            else:
                sportplan.append(f"{tag}: Keine geeigneten Aktivitäten verfügbar")
        return sportplan

    def zeige_sportplan(self, sportplan):
        sportplan_window = tk.Toplevel(self)
        sportplan_window.title("Ihr Sportplan")
        sportplan_window.geometry('1300x700')
        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        sportplan_window.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(sportplan_window, image=sportplan_window.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        Label(sportplan_window, text="\n".join(sportplan), font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
              fg='#333333', justify="center").pack(padx=80, pady=200)

    def zurück(self):
        self.withdraw()
        self.master.zunehmen.deiconify()




class Diätplan_Sportplan(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title('Diätplan und sportplan fürs Zunehmnen')
        self.geometry('1300x700')

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        self.mahlzeiten_db = self.lese_mahlzeiten_csv()

        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(relx=0.5, rely=0.6,height=400,width=500, anchor='center')

        # zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
        #                        command=self.zurück, font=('Helvetica', 12))
        # zurück_button.pack(fill=tk.BOTH, expand=True)

        label_text = "Geben Sie Unverträglichkeiten ein (durch Komma getrennt):"
        Label(frame, text=label_text, font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333').grid(row=0, column=1, padx=2, pady=2)

        self.unverträglichkeiten_entry = Entry(frame, font=('Helvetica', 12), width=30)
        self.unverträglichkeiten_entry.grid(row=1, column=1, padx=2, pady=2)

        tk.Label(frame, text="Einschränkungen:", font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333').grid(row=3, column=0, padx=2, pady=2)
        self.einschränkungen_entry = tk.Entry(frame, font=('Helvetica', 12))
        self.einschränkungen_entry.grid(row=3, column=1, padx=2, pady=2)

        sportplan_button = Button(frame, text='Sportplan und Diätplan generieren', bg='#4CAF50', fg='#F5F5F5', command=self.berechne_kalorienbedarf, font=('Helvetica', 12))
        sportplan_button.grid(row=5, column=1, columnspan=2, pady=(20, 10))

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5', command=self.zurück, font=('Helvetica', 12))
        zurück_button.grid(row=7, column=1, columnspan=2, pady=(10, 20))

    def lese_mahlzeiten_csv(self):
        dateipfad = r'zutaten.csv2'
        mahlzeiten_db = {}
        with open(dateipfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                typ = zeile['Typ']
                mahlzeit = zeile['Mahlzeit']
                kalorien = int(zeile['Kalorien'])
                zutaten = zeile['Zutaten'].split('|')
                if typ not in mahlzeiten_db:
                    mahlzeiten_db[typ] = []
                mahlzeiten_db[typ].append((mahlzeit, kalorien, zutaten))
        return mahlzeiten_db


    def berechne_kalorienbedarf(self):
        # Zugriff auf Benutzerdaten aus dem Hauptfenster
        geschlecht = self.master.benutzer_daten['Geschlecht']
        große = float(self.master.benutzer_daten['Körpergröße'])
        gewicht = float(self.master.benutzer_daten['Gewicht'])
        alter = int(self.master.benutzer_daten['Alter'])

        if geschlecht == "Männlich":
            self.bmr_zunehmen = (88.362 + (13.397 * gewicht) + (4.799 * große) - (5.677 * alter))+500
        else:
            self.bmr_zunehmen = (447.593 + (9.247 * gewicht) + (3.098 * große) - (4.330 * alter))+500

        self.bmr_zunehmen_int = int(self.bmr_zunehmen)
        self.handle_sportplan_button()

    def handle_sportplan_button(self):
        # Holen der Benutzereinschränkungen als Liste
        sportaktivitäten = self.lese_sport_csv()
        unverträglichkeiten_text = self.unverträglichkeiten_entry.get()
        unverträglichkeiten_liste = [item.strip() for item in unverträglichkeiten_text.split(',') if item]
        self.master.benutzer_daten['Unverträglichkeiten'] = unverträglichkeiten_liste
        benutzer_einschränkungen = self.einschränkungen_entry.get().split(',')
        sportplan = self.erstelle_sportplan(sportaktivitäten, benutzer_einschränkungen)
        diätplan = self.erstelle_diätplan( self.bmr_zunehmen_int,self.master.benutzer_daten)
        self.zeige_diatplan_sportplan(sportplan , diätplan)

    def wähle_mahlzeit(self, mahlzeiten, zielkalorien, unverträglichkeiten):
        passende_mahlzeiten = [m for m in mahlzeiten if
                               m[1] <= zielkalorien and not any(allergen in m[2] for allergen in unverträglichkeiten)]
        return random.choice(passende_mahlzeiten) if passende_mahlzeiten else ("Nicht verfügbar", 0)

    def erstelle_diätplan(self, kalorienbedarf, benutzerdaten):
        unverträglichkeiten = benutzerdaten.get('Unverträglichkeiten', [])
        self.diätplan = {"Frühstück": None, "Mittagessen": None, "Abendessen": None,
                    "Desserts": None, "Zwischenmahlzeiten": None }

        for mahlzeit_typ in self.diätplan.keys():
            try:
                verfügbare_mahlzeiten = self.mahlzeiten_db[mahlzeit_typ]
                mahlzeit = self.wähle_mahlzeit(verfügbare_mahlzeiten, kalorienbedarf / 3, unverträglichkeiten)
                self.diätplan[mahlzeit_typ] = mahlzeit
            except KeyError:
                print(f"Warnung: Keine Daten für {mahlzeit_typ} gefunden")

        return self.diätplan


    def lese_sport_csv(self):
        self.sport_csv_pfad = r'Wochenplan für Sport'
        sport_db = {}
        with open(self.sport_csv_pfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                tag = zeile['Tag'].strip()
                if tag not in sport_db:
                    sport_db[tag] = []
                sport_db[tag].append(zeile)
        return sport_db

    def erstelle_sportplan(self, sportaktivitäten, benutzer_einschränkungen):
        self.sportplan = []
        for tag, aktivitäten in sportaktivitäten.items():
            # Überprüfung, ob Benutzereinschränkungen vorhanden sind
            if benutzer_einschränkungen and not all(e.strip() == '' for e in benutzer_einschränkungen):
                # Filtern der Aktivitäten, die nicht mit den Einschränkungen übereinstimmen
                geeignete_aktivitäten = [a for a in aktivitäten if all(
                    einschränkung.lower().strip() not in a['Einschränkungen'].lower() for einschränkung in
                    benutzer_einschränkungen)]
            else:
                # Keine Benutzereinschränkungen, alle Aktivitäten sind geeignet
                geeignete_aktivitäten = aktivitäten

            if geeignete_aktivitäten:
                gewählte_aktivität = random.choice(geeignete_aktivitäten)
                self.sportplan.append(f"{tag}: {gewählte_aktivität['Aktivität']} ({gewählte_aktivität['Dauer']})")
            else:
                self.sportplan.append(f"{tag}: Keine geeigneten Aktivitäten verfügbar")
        return self.sportplan

    def zeige_diatplan_sportplan(self, diätplan, sportplan):
        plan_window = tk.Toplevel(self)
        plan_window.title("Ihr Plan")
        plan_window.geometry('1300x700')
        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        self.sport_csv_pfad = r'Wochenplan für Sport'
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        plan_window.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(plan_window, image=plan_window.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        frame = tk.Frame(plan_window, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(relx=0.5, rely=0.6,height=400,width=500, anchor='center')


        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                                command=self.zurück2, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True, pady=(10, 20))

        gewichtdifferenz =int(Wunschgewicht_variable_zunehmen) - int(Gewicht)
        Zeit_bis_Wunschgewicht = gewichtdifferenz / 0.5

        Label(frame, text="\n".join(self.sportplan), font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
              fg='#333333', justify="center").grid(row=6, column=0, pady=(10, 20))


        diätplan_text = (
            f"Ihr täglicher Kalorienbedarf, um {gewichtdifferenz} Kg zu erreichen, beträgt: {self.bmr_zunehmen_int:.2f} kcal\n"
            f"Sie erreichen Ihr Wunschgewicht in {Zeit_bis_Wunschgewicht} Wochen\n\nDiätplan:\n")

        MAX_ITERATIONS = 100  # Maximale Anzahl von Iterationen zur Generierung eines neuen Plans
        iteration_count = 0    #Iterationszähler

        while True:
            self.gesamtkalorien_mahlzeiten = sum(
                mahlzeit_info[1] for mahlzeit_info in self.diätplan.values() if mahlzeit_info and mahlzeit_info[0]
            )

            if (self.bmr_zunehmen_int - 200) <= self.gesamtkalorien_mahlzeiten <= (self.bmr_zunehmen_int + 200):
                for mahlzeit_typ, mahlzeit_info in self.diätplan.items():
                    if mahlzeit_info is None or mahlzeit_info[0] is None:
                        diätplan_text += f"{mahlzeit_typ}: Nicht verfügbar\n"
                    else:
                        diätplan_text += f"{mahlzeit_typ}: {mahlzeit_info[0]} ({mahlzeit_info[1]} kcal)\n"

                Textfeld = Label(frame, text=diätplan_text, font=("Cooper Black", 13, "italic bold"),
                             bg='#7CCD7C',fg='#333333', justify="center")
                Textfeld.grid(row=0, column=0, pady=(20, 10))
                #Textfeld.pack(padx=80, pady=200)

                break

            else:
        # # ein neuer Versuch einer Erzeugung eines Diätplan
                diätplan = self.erstelle_diätplan(self.bmr_zunehmen_int, self.master.benutzer_daten)
                iteration_count += 1

            if iteration_count >= MAX_ITERATIONS:
                 print("Maximale Anzahl von Iterationen erreicht. Es konnte kein Plan gefunden werden.")
                 break
    def zurück2(self):
         self.withdraw()
         self.master.diätplan_sportplan_ab.deiconify()


    def zurück(self):
            self.withdraw()
            self.master.zunehmen.deiconify()


class Diätplan_ab(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title('Diätplan fürs Abnehmen')
        self.geometry('1300x700')

        self.mahlzeiten_db = self.lese_mahlzeiten_csv()

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=350, y=500)

        berechnen_button = Button(frame, text='Kalorienbedarf berechnen', bg='#4CAF50', fg='#F5F5F5',
                                  command=self.berechne_kalorienbedarf, font=('Helvetica', 12))
        berechnen_button.pack(fill=tk.BOTH, expand=True, pady=(20, 10))  # Abstand oben 20, unten 10

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                               command=self.zurück, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True, pady=(10, 20))  # Abstand oben 10, unten 20

        label_text = "Geben Sie Unverträglichkeiten ein (durch Komma getrennt):"
        Label(frame, text=label_text, font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333').pack()

        self.unverträglichkeiten_entry = Entry(frame, font=('Helvetica', 12), width=30)
        self.unverträglichkeiten_entry.pack(pady=(10, 10))

    def lese_mahlzeiten_csv(self):
        dateipfad = r'zutaten.csv2'
        mahlzeiten_db = {}
        with open(dateipfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                typ = zeile['Typ']
                mahlzeit = zeile['Mahlzeit']
                kalorien = int(zeile['Kalorien'])
                zutaten = zeile['Zutaten'].split('|')
                if typ not in mahlzeiten_db:
                    mahlzeiten_db[typ] = []
                mahlzeiten_db[typ].append((mahlzeit, kalorien, zutaten))
        return mahlzeiten_db
    def berechne_kalorienbedarf(self):
        # Zugriff auf Benutzerdaten aus dem Hauptfenster
        geschlecht = self.master.benutzer_daten['Geschlecht']
        große = float(self.master.benutzer_daten['Körpergröße'])
        gewicht = float(self.master.benutzer_daten['Gewicht'])
        alter = int(self.master.benutzer_daten['Alter'])

        if geschlecht == "Männlich":
            bmr_abnehmen = (88.362 + (13.397 * gewicht) + (4.799 * große) - (5.677 * alter))-500
        else:
            bmr_abnehmen = (447.593 + (9.247 * gewicht) + (3.098 * große) - (4.330 * alter))-500

        unverträglichkeiten_text = self.unverträglichkeiten_entry.get()
        unverträglichkeiten_liste = [item.strip() for item in unverträglichkeiten_text.split(',') if item]

        self.bmr_abnehmen_int = int(bmr_abnehmen)

        # Update der Benutzerdaten
        self.master.benutzer_daten['Unverträglichkeiten'] = unverträglichkeiten_liste
        diätplan = self.erstelle_diätplan(bmr_abnehmen, self.master.benutzer_daten)
        self.zeige_plan(bmr_abnehmen, diätplan)
        self.withdraw()

    def wähle_mahlzeit(self, mahlzeiten, zielkalorien, unverträglichkeiten):
        passende_mahlzeiten = [m for m in mahlzeiten if
                               m[1] <= zielkalorien and not any(allergen in m[2] for allergen in unverträglichkeiten)]
        return random.choice(passende_mahlzeiten) if passende_mahlzeiten else ("Nicht verfügbar", 0)

    def erstelle_diätplan(self, kalorienbedarf, benutzerdaten):
        unverträglichkeiten = benutzerdaten.get('Unverträglichkeiten', [])
        diätplan = {"Frühstück": None, "Mittagessen": None, "Abendessen": None,
                    "Desserts": None, "Zwischenmahlzeiten": None }

        for mahlzeit_typ in diätplan.keys():
            try:
                verfügbare_mahlzeiten = self.mahlzeiten_db[mahlzeit_typ]
                mahlzeit = self.wähle_mahlzeit(verfügbare_mahlzeiten, kalorienbedarf / 3, unverträglichkeiten)
                diätplan[mahlzeit_typ] = mahlzeit
            except KeyError:
                print(f"Warnung: Keine Daten für {mahlzeit_typ} gefunden")

        return diätplan

    def zeige_plan(self, bmr_abnehmen_int, diätplan):
        plan_window = tk.Toplevel(self)
        plan_window.title("Ihr Plan")
        plan_window.geometry('1300x700')
        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        plan_window.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(plan_window, image=plan_window.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        frame = tk.Frame(plan_window, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=350, y=500)

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
                                command=self.zurück2, font=('Helvetica', 12))
        zurück_button.pack(fill=tk.BOTH, expand=True, pady=(10, 20))

        gewichtdifferenz =  int(Gewicht) - int(Wunschgewicht_variable_abnehmen)
        Zeit_bis_Wunschgewicht = gewichtdifferenz / 0.5

        diätplan_text = (
            f"Ihr täglicher Kalorienbedarf, um {gewichtdifferenz} Kg zu erreichen, beträgt: {bmr_abnehmen_int:.2f} kcal\n"
            f"Sie erreichen Ihr Wunschgewicht in {Zeit_bis_Wunschgewicht} Wochen\n\nDiätplan:\n")

        MAX_ITERATIONS = 100
        iteration_count = 0

        while True:
            self.gesamtkalorien_mahlzeiten = sum(
                mahlzeit_info[1] for mahlzeit_info in diätplan.values() if mahlzeit_info and mahlzeit_info[0]
            )

            if (self.bmr_abnehmen_int - 200) <= self.gesamtkalorien_mahlzeiten <= (self.bmr_abnehmen_int + 200):
                for mahlzeit_typ, mahlzeit_info in diätplan.items():
                    if mahlzeit_info is None or mahlzeit_info[0] is None:
                        diätplan_text += f"{mahlzeit_typ}: Nicht verfügbar\n"
                    else:
                        diätplan_text += f"{mahlzeit_typ}: {mahlzeit_info[0]} ({mahlzeit_info[1]} kcal)\n"

                Textfeld = Label(plan_window, text=diätplan_text, font=("Cooper Black", 13, "italic bold"),
                                 bg='#7CCD7C',
                                 fg='#333333', justify="center")
                Textfeld.pack(padx=80, pady=200)

                break

            else:
         # Générer un nouveau plan
                diätplan = self.erstelle_diätplan(self.bmr_abnehmen_int, self.master.benutzer_daten)
                iteration_count += 1

            if iteration_count >= MAX_ITERATIONS:
                print("Maximale Anzahl von Iterationen erreicht. Es konnte kein Plan gefunden werden.")
                break

    def zurück2(self):
        self.withdraw()
        self.master.diätplan_ab.deiconify()

    def zurück(self):
        self.withdraw()
        self.master.abnehmen.deiconify()


class Sportplan_ab(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title('Sportplan fürs Abnehmen')
        self.geometry('1300x700')
        self.sport_csv_pfad = r'Wochenplan für Sport'
        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        frame = tk.Frame(self, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(x=500, y=500)

        tk.Label(frame, text="Einschränkungen:", font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333').grid(row=0, column=0, padx=7, pady=7)
        self.einschränkungen_entry = tk.Entry(frame, font=('Helvetica', 12))
        self.einschränkungen_entry.grid(row=0, column=1, padx=2, pady=2)

        sportplan_button = Button(frame, text='Sportplan generieren', bg='#4CAF50', fg='#F5F5F5', command=self.handle_sportplan_button, font=('Helvetica', 12))
        sportplan_button.grid(row=1, column=0, columnspan=2, pady=(20, 10))

        zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5', command=self.zurück, font=('Helvetica', 12))
        zurück_button.grid(row=2, column=0, columnspan=2, pady=(10, 20))

    def handle_sportplan_button(self):
        # Holen der Benutzereinschränkungen als Liste
        benutzer_einschränkungen = self.einschränkungen_entry.get().split(',')
        self.generiere_sportplan(benutzer_einschränkungen)
    def lese_sport_csv(self):
        sport_db = {}
        with open(self.sport_csv_pfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                tag = zeile['Tag'].strip()
                if tag not in sport_db:
                    sport_db[tag] = []
                sport_db[tag].append(zeile)
        return sport_db

    def generiere_sportplan(self, benutzer_einschränkungen):
        sportaktivitäten = self.lese_sport_csv()
        sportplan = self.erstelle_sportplan(sportaktivitäten, benutzer_einschränkungen)
        self.zeige_sportplan(sportplan)

    def erstelle_sportplan(self, sportaktivitäten, benutzer_einschränkungen):
        sportplan = []
        for tag, aktivitäten in sportaktivitäten.items():
            # Überprüfung, ob Benutzereinschränkungen vorhanden sind
            if benutzer_einschränkungen and not all(e.strip() == '' for e in benutzer_einschränkungen):
                # Filtern der Aktivitäten, die nicht mit den Einschränkungen übereinstimmen
                geeignete_aktivitäten = [a for a in aktivitäten if all(
                    einschränkung.lower().strip() not in a['Einschränkungen'].lower() for einschränkung in
                    benutzer_einschränkungen)]
            else:
                # Keine Benutzereinschränkungen, alle Aktivitäten sind geeignet
                geeignete_aktivitäten = aktivitäten

            if geeignete_aktivitäten:
                gewählte_aktivität = random.choice(geeignete_aktivitäten)
                sportplan.append(f"{tag}: {gewählte_aktivität['Aktivität']} ({gewählte_aktivität['Dauer']})")
            else:
                sportplan.append(f"{tag}: Keine geeigneten Aktivitäten verfügbar")
        return sportplan

    def zeige_sportplan(self, sportplan):
        sportplan_window = tk.Toplevel(self)
        sportplan_window.title("Ihr Sportplan")
        sportplan_window.geometry('1300x700')
        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        sportplan_window.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(sportplan_window, image=sportplan_window.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        Label(sportplan_window, text="\n".join(sportplan), font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
              fg='#333333', justify="center").pack(padx=80, pady=200)

    def zurück(self):
        self.withdraw()
        self.master.abnehmen.deiconify()




class Diätplan_Sportplan_ab(tk.Toplevel):
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.withdraw()

        self.title('Diätplan und Sportplan fürs Abnehmen')
        self.geometry('1300x700')

        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        self.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(self, image=self.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        self.mahlzeiten_db = self.lese_mahlzeiten_csv()

        Text = tk.Label(self, text="Diätplan und Sportplan", font=("Cooper Black", 17, "italic bold"), bg="#383838",
                        fg="#F5F5F5")
        Text.place(x=510, y=200)

        # Zentraler Rahmen für Eingabefelder
        zentral_rahmen = tk.Frame(self, bg="#383838")
        zentral_rahmen.place(relx=0.5, rely=0.8, height=300, width=500, anchor='center')

        # Label und Eingabefeld für Unverträglichkeiten
        self.create_label_entry_pair(zentral_rahmen, "Unverträglichkeiten:", 0)

        # Label und Eingabefeld für Einschränkungen
        self.create_label_entry_pair(zentral_rahmen, "Einschränkungen:", 2)

        # Schaltfläche für Sportplan und Diätplan
        self.create_button(zentral_rahmen, 'Sportplan und Diätplan generieren', self.berechne_kalorienbedarf, 4)

        # Schaltfläche zum Zurückkehren
        self.create_button(zentral_rahmen, 'Zurück', self.zurück, 5)

    def create_label_entry_pair(self, frame, text, row):
        Label(frame, text=text, font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C', fg='#333333').grid(row=row,
                                                                                                           column=0,
                                                                                                           padx=7,
                                                                                                           pady=7,
                                                                                                           sticky='e')
        entry = Entry(frame, font=('Helvetica', 12), width=30)
        entry.grid(row=row, column=1, padx=7, pady=7)

    def create_button(self, frame, text, command, row):
        button = Button(frame, text=text, bg='#4CAF50', fg='#F5F5F5', command=command, font=('Helvetica', 12))
        button.grid(row=row, column=0, columnspan=2, pady=10, sticky='ew')
    def lese_mahlzeiten_csv(self):
        dateipfad = r'zutaten.csv2'
        mahlzeiten_db = {}
        with open(dateipfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                typ = zeile['Typ']
                mahlzeit = zeile['Mahlzeit']
                kalorien = int(zeile['Kalorien'])
                zutaten = zeile['Zutaten'].split('|')
                if typ not in mahlzeiten_db:
                    mahlzeiten_db[typ] = []
                mahlzeiten_db[typ].append((mahlzeit, kalorien, zutaten))
        return mahlzeiten_db


    def berechne_kalorienbedarf(self):
        # Zugriff auf Benutzerdaten aus dem Hauptfenster
        geschlecht = self.master.benutzer_daten['Geschlecht']
        große = float(self.master.benutzer_daten['Körpergröße'])
        gewicht = float(self.master.benutzer_daten['Gewicht'])
        alter = int(self.master.benutzer_daten['Alter'])

        if geschlecht == "Männlich":
            self.bmr_abnehmen = (88.362 + (13.397 * gewicht) + (4.799 * große) - (5.677 * alter))-500
        else:
            self.bmr_abnehmen = (447.593 + (9.247 * gewicht) + (3.098 * große) - (4.330 * alter))-500

        self.bmr_abnehmen_int = int(self.bmr_abnehmen)
        self.handle_sportplan_button()

    def handle_sportplan_button(self):
        # Holen der Benutzereinschränkungen als Liste
        sportaktivitäten = self.lese_sport_csv()
        unverträglichkeiten_text = self.unverträglichkeiten_entry.get()
        unverträglichkeiten_liste = [item.strip() for item in unverträglichkeiten_text.split(',') if item]
        self.master.benutzer_daten['Unverträglichkeiten'] = unverträglichkeiten_liste
        benutzer_einschränkungen = self.einschränkungen_entry.get().split(',')
        sportplan = self.erstelle_sportplan(sportaktivitäten, benutzer_einschränkungen)
        diätplan = self.erstelle_diätplan( self.bmr_abnehmen_int,self.master.benutzer_daten)
        self.zeige_diatplan_sportplan(sportplan , diätplan)

    def wähle_mahlzeit(self, mahlzeiten, zielkalorien, unverträglichkeiten):
        passende_mahlzeiten = [m for m in mahlzeiten if
                               m[1] <= zielkalorien and not any(allergen in m[2] for allergen in unverträglichkeiten)]
        return random.choice(passende_mahlzeiten) if passende_mahlzeiten else ("Nicht verfügbar", 0)

    def erstelle_diätplan(self, kalorienbedarf, benutzerdaten):
        unverträglichkeiten = benutzerdaten.get('Unverträglichkeiten', [])
        self.diätplan = {"Frühstück": None, "Mittagessen": None, "Abendessen": None,
                    "Desserts": None, "Zwischenmahlzeiten": None }

        for mahlzeit_typ in self.diätplan.keys():
            try:
                verfügbare_mahlzeiten = self.mahlzeiten_db[mahlzeit_typ]
                mahlzeit = self.wähle_mahlzeit(verfügbare_mahlzeiten, kalorienbedarf / 3, unverträglichkeiten)
                self.diätplan[mahlzeit_typ] = mahlzeit
            except KeyError:
                print(f"Warnung: Keine Daten für {mahlzeit_typ} gefunden")

        return self.diätplan


    def lese_sport_csv(self):
        self.sport_csv_pfad = r'Wochenplan für Sport'
        sport_db = {}
        with open(self.sport_csv_pfad, mode='r', encoding='utf-8') as datei:
            reader = csv.DictReader(datei)
            for zeile in reader:
                tag = zeile['Tag'].strip()
                if tag not in sport_db:
                    sport_db[tag] = []
                sport_db[tag].append(zeile)
        return sport_db

    def erstelle_sportplan(self, sportaktivitäten, benutzer_einschränkungen):
        self.sportplan = []
        for tag, aktivitäten in sportaktivitäten.items():
            # Überprüfung, ob Benutzereinschränkungen vorhanden sind
            if benutzer_einschränkungen and not all(e.strip() == '' for e in benutzer_einschränkungen):
                # Filtern der Aktivitäten, die nicht mit den Einschränkungen übereinstimmen
                geeignete_aktivitäten = [a for a in aktivitäten if all(
                    einschränkung.lower().strip() not in a['Einschränkungen'].lower() for einschränkung in
                    benutzer_einschränkungen)]
            else:
                # Keine Benutzereinschränkungen, alle Aktivitäten sind geeignet
                geeignete_aktivitäten = aktivitäten

            if geeignete_aktivitäten:
                gewählte_aktivität = random.choice(geeignete_aktivitäten)
                self.sportplan.append(f"{tag}: {gewählte_aktivität['Aktivität']} ({gewählte_aktivität['Dauer']})")
            else:
                self.sportplan.append(f"{tag}: Keine geeigneten Aktivitäten verfügbar")
        return self.sportplan

    def zeige_diatplan_sportplan(self, diätplan, sportplan):
        plan_window = tk.Toplevel(self)
        plan_window.title("Ihr Plan")
        plan_window.geometry('1300x700')
        hintergrund_link = r"Hintergrund_mit_logo.jpg"
        self.sport_csv_pfad = r'Wochenplan für Sport'
        hintergrund_bild = Image.open(hintergrund_link)
        hintergrund_bild = hintergrund_bild.resize((1300, 700), Image.LANCZOS)
        plan_window.hintergrund_bild = ImageTk.PhotoImage(hintergrund_bild)
        Etikett_bild = Label(plan_window, image=plan_window.hintergrund_bild)
        Etikett_bild.place(x=0, y=0, relwidth=1, relheight=1)

        frame = tk.Frame(plan_window, bd=5, relief=tk.SOLID, borderwidth=5, bg='#383838')
        frame.place(relx=0.5, rely=0.5, anchor='center')


        # zurück_button = Button(frame, text='Zurück', bg='#4CAF50', fg='#F5F5F5',
        #                        command=self.zurück2, font=('Helvetica', 12))
        # zurück_button.pack(fill=tk.BOTH, expand=True, pady=(10, 20))

        gewichtdifferenz =  int(Gewicht) - int(Wunschgewicht_variable_abnehmen)
        Zeit_bis_Wunschgewicht = gewichtdifferenz / 0.5

        Label(frame, text="\n".join(self.sportplan), font=("Cooper Black", 13, "italic bold"), bg='#7CCD7C',
              fg='#333333', justify="center").grid(row=6, column=0, pady=(10, 20))


        diätplan_text = (
            f"Ihr täglicher Kalorienbedarf, um {gewichtdifferenz} Kg zu erreichen, beträgt: {self.bmr_abnehmen_int:.2f} kcal\n"
            f"Sie erreichen Ihr Wunschgewicht in {Zeit_bis_Wunschgewicht} Wochen\n\nDiätplan:\n")

        MAX_ITERATIONS = 100
        iteration_count = 0
        while True:
            self.gesamtkalorien_mahlzeiten = sum(
                mahlzeit_info[1] for mahlzeit_info in self.diätplan.values() if mahlzeit_info and mahlzeit_info[0]
            )

            if (self.bmr_abnehmen_int - 200) <= self.gesamtkalorien_mahlzeiten <= (self.bmr_abnehmen_int + 200):
                for mahlzeit_typ, mahlzeit_info in self.diätplan.items():
                    if mahlzeit_info is None or mahlzeit_info[0] is None:
                        diätplan_text += f"{mahlzeit_typ}: Nicht verfügbar\n"
                    else:
                        diätplan_text += f"{mahlzeit_typ}: {mahlzeit_info[0]} ({mahlzeit_info[1]} kcal)\n"

                Textfeld = Label(frame, text=diätplan_text, font=("Cooper Black", 13, "italic bold"),
                                 bg='#7CCD7C',
                                 fg='#333333', justify="center")
                Textfeld.grid(row=0, column=0, pady=(20, 10))

                break

            else:
         # Générer un nouveau plan
                diätplan = self.erstelle_diätplan(self.bmr_abnehmen_int, self.master.benutzer_daten)
                iteration_count += 1

            if iteration_count >= MAX_ITERATIONS:
                print("Maximale Anzahl von Iterationen erreicht. Es konnte kein Plan gefunden werden.")
                break

    # def zurück2(self):
    #     self.withdraw()
    #     self.master.diätplan_sportplan_ab.deiconify()



    def zurück(self):
        self.withdraw()
        self.master.abnehmen.deiconify()

if __name__ == "__main__":
    app = Applikation()
    app.mainloop()
